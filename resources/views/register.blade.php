<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SanberBook | Sign Up Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
      @csrf
      <label for="firstName">First name:</label><br /><br />
      <input id="firstName" name="firstName" type="text" /><br /><br />

      <label for="lastName">Last name:</label><br /><br />
      <input id="lastName" name="lastName" type="text" /><br /><br />

      <label for="gender">Gender:</label><br /><br />
      <input type="radio" name="gender" id="male" value="male" />
      <label for="male">Male</label><br />
      <input type="radio" name="gender" id="female" value="female" />
      <label for="female">Female</label><br />
      <input type="radio" name="gender" id="otherGender" value="otherGender" />
      <label for="otherGender">Other</label><br /><br />

      <label for="nationality">Nationality:</label><br /><br />
      <select name="nationality" id="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="english">English</option>
        <option value="italian">Italian</option></select
      ><br /><br />

      <label for="language">Language Spoken:</label><br /><br />
      <input type="checkbox" name="bahasa" id="bahasa" value="bahasa" />
      <label for="bahasa">Bahasa Indonesia</label><br />
      <input type="checkbox" name="english" id="english" value="english" />
      <label for="english">English</label><br />
      <input
        type="checkbox"
        name="otherLanguage"
        id="otherLanguage"
        value="otherLanguage"
      />
      <label for="otherLanguage">Other</label><br /><br />

      <label for="bio">Bio:</label><br /><br />
      <textarea name="bio" id="bio" cols="40" rows="10"></textarea><br /><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
